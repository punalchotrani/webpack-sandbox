// Using a constructor to create an Object


// function Person(firstName, lastName) {
// 	this.firstName = firstName;
// 	this.lastName = lastName;
// 	// this.fullName = () => { 
// 	// 	return `${this.firstName} ${this.lastName}`;
// 	// };
// 	//console.log(this);
// }

// Person.prototype.fullname = function (firstName, lastName) {
// 	return `${this.firstName} ${this.lastName}`;
// };

// export const punal = new Person('Punal', 'Chotrani');

// export const geetu = new Person('Geetanjali', 'Tuli');


// //Built in constructor
// export const name1 = 'Tony';
// export const name2 = new String('Tony');

//import { superHeroData } from './components/data';

//console.log(superHeroData);

class Avenger {
	constructor(superhero, publisher, alter_ego, first_appearance, characters) {
		this.superhero = superhero;
		this.publisher = publisher;
		this.alter_ego = alter_ego;
		this.first_appearance = first_appearance;
		this.characters = characters;
	}

	tagline() {
		return `I'm ${this.superhero}`;
	}
};


//sub class of person using super()
class Color extends Avenger {
	constructor(superhero, publisher, alter_ego, first_appearance, characters, primary_color) {
		super(superhero, publisher, alter_ego, first_appearance, characters);
		this.primary_color = primary_color;
	}
};

// export const blackPanther = new Color('Black Panther', 'Marvel Comics', 'T\'Challa', 'Black Panther #1', 'T\'Challa', 'Black');

// export const blackPantherTagline = blackPanther.tagline();




//export default { punal, geetu };
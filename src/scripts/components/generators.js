import { superHeroData_small } from './data';

// const superNames = superHeroIterater(superHeroData_small);

// function superHeroIterater(names) {
// 	let nextName = 0;

// 	return {
// 		next: () => {
// 			if (nextName >= names.length) {
// 				nextName = 0;
// 			}
// 			return { value: names[nextName++], done: false };
// 		}
// 	};
// }
// nextSuperName();

// export function nextSuperName() {
// 	const currentSuperName = superNames.next().value;

// 	const displayImage = document.getElementById('displayImage');
// 	const displayDetails = document.getElementById('details');

// 	if (currentSuperName !== undefined) {
// 		displayImage.innerHTML = `
// 		<img class="responsive-img" src="${currentSuperName.image}" />
// 	`;

// 		displayDetails.innerHTML = `
// 		<ul class="collection with-header left-align">
// 			<li class="collection-header"><h4>${currentSuperName.superhero}</h4></li>
// 			<li class="collection-item"><strong>Alter Ego:</strong> ${
// 				currentSuperName.alter_ego
// 			}</li>
// 			<li class="collection-item"><strong>Publisher:</strong> ${
// 				currentSuperName.publisher
// 			}</li>
// 			<li class="collection-item"><strong>First Appearance:</strong> ${
// 				currentSuperName.first_appearance
// 			}</li>
// 		</ul>
// 	`;
// 	} else {
// 		nextSuperName();
// 		//window.location.reload();
// 	}
// 	console.log(currentSuperName);
// }

// const arr = ['Peter', 'Paul', 'Mary'];

// function* generator(i) {
// 	while (true) {
// 		if (i >= arr.length) {
// 			i = 0;
// 		}

// 		yield arr[i];
// 		i++;
// 	}
// }

// var gen = generator(0);

// console.log(gen.next().value);
// console.log(gen.next().value);
// console.log(gen.next().value);
// console.log(gen.next().value);

function* superHeroGenerator(i) {
	while (true) {
		if (i >= superHeroData_small.length) {
			i = 0;
		}
		yield superHeroData_small[i];
		i++;
	}
}

let heroImage = superHeroGenerator(0);
let heroName = superHeroGenerator(0);
let heroAlterEgo = superHeroGenerator(0);
let heroPublisher = superHeroGenerator(0);
let heroFirstAppearance = superHeroGenerator(0);

export function getDetails() {
	const displayImage = document.getElementById('displayImage');
	const details = document.getElementById('details');

	displayImage.innerHTML = `<img class="responsive-img" src="${
		heroImage.next().value.image
	}" />`;
	details.innerHTML = `
		<ul class="collection with-header">
			<li class="collection-header"><h4>${heroName.next().value.superhero}</h4></li>
			<li class="collection-item">${heroAlterEgo.next().value.alter_ego}</li>
			<li class="collection-item">${heroPublisher.next().value.publisher}</li>
			<li class="collection-item">${
				heroFirstAppearance.next().value.first_appearance
			}</li>
		</ul>`;
}

getDetails();

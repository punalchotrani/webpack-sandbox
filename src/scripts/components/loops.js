// FOR LOOP
/* let loop = () => {
  for (let i = 1; i <= 10; i++) {
    if (i === 4) {
      console.log(`${i} 🤩`);
      continue;
    }
    if (i === 8) {
      console.log(`${i} ❌`);
      break;
    }
    console.log(i);
  }
}; */


// WHILE LOOP
/* let loop = () => {
  let i = 1;
  while (i <= 10) {
    console.log(i);
    i++;
  }
}; */


// DO WHILE // This will loop atleat once.
/* let loop = () => {
  let result = "";
  let i = 1;

  do {
    i++;
    result = result + i;
  } while (i < 5);
  console.log(result);
}; */

let avengers = ['Ironman', 'Captain-America', 'Spiderman', 'Thor', 'Hulk'];

/* let loop = () => {
  for (let i = 0; i < avengers.length; i++) {
    console.log(avengers[i]);
  }
}; */

//let loop = () => avengers.forEach((el, index) => `${index} : ${el}`);



// let createObj = Object.assign({}, avengers);

let createObj = () => {
	avengers.forEach(function (avenger) {
		return avenger;
	});
};
export default createObj;
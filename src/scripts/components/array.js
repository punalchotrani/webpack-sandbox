const numbers = [96, 42, 22, 33, 5, 98, 67, 56, 88];
let val;

//get array length
//val = numbers.length;

//val = Array.isArray(numbers); //Array => is the object, isArray() is the object method for Array


//Find index of a value
//val = numbers.indexOf(88);

//Mutationg Arrays
// numbers.push(91); //Adds to the end of the array

// numbers.unshift(51); //Adds to the front of the array

// numbers.pop(); //Removes item from the back of the array

// numbers.reverse(); //Reverses the order of the array.

//const firstItem = numbers.shift(); //Removes items from the front of the array and returns the removed item.

//console.log(firstItem);

//numbers.splice(0, 7); // Removes items in an array with a range. In this case 0 to 7 of the index array.


//numbers.sort((x, y) => x - y); //This is using a callback, compare function

//numbers.sort((x, y) => y - x); //This is using a callback, compare function, reverse order

val = numbers.find(element => element < 50);

export { val, numbers };
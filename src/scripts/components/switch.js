let color = 'black';
let val;

function getColor() {
  switch (color) {
    case 'orange':
      return 'The color is orange';
      break;
    case 'red':
      return 'The color is red';
      break;
    default:
      return 'No matching color';
  }
}

val = getColor();

export default val;
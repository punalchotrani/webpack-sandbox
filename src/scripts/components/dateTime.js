const today = new Date();
let val;

val = today;
val = today.getDate(); // Get the current date
val = today.getMonth() + 1; // Get the current month
val = today.getFullYear(); // Get the current year
val = today.getDay(); //Get the current day
val = today.getHours(); //Get the current hours
val = today.getMinutes(); //Get the current minutes
val = today.getSeconds(); //Get current seconds
val = today.getMilliseconds(); //Get current miliseconds
val = today.getTime(); // Get current time

export default val;
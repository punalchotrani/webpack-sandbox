// //import superHeroData from './data';

// export const getData = () => {

// 	const xhr = new XMLHttpRequest();

// 	//open
// 	xhr.open('GET', './src/scripts/components/data.js', true);
// 	xhr.onload = function () {
// 		if (this.status === 200) {

// 			const parsedData = JSON.parse(
// 				xhr.response
// 					.replace('const superHeroData = [', '[')
// 					.replace('];', ']')
// 					.replace(/'/g, '"')
// 					.replace(/ /g, '')
// 			);

// 			let output = '';

// 			parsedData.forEach(superstar => {
// 				output += `
// 					<div class="categories-container pin-top">
// 						<ul class="categories db">
// 							<li><strong>Superhero</strong> : ${superstar.superhero}</li>
// 							<li><strong>Publisher</strong> : ${superstar.publisher}</li>
// 							<li><strong>Alter-Ego</strong> : ${superstar.alter_ego}</li>
// 							<li><strong>First Appearance</strong> : ${superstar.first_appearance}</li>
// 							<li><strong>Characters</strong> : ${superstar.characters}</li>
// 						</ul>
// 					</div>`;
// 			});

// 			document.getElementById('output').innerHTML = output;

// 		}
// 	};
// 	xhr.send();
// };


//Asyns await

export async function sayGreeting(greeting) {
	const promise = new Promise((res, reg) => {
		setTimeout(() => res(greeting), 5000);
	});

	const resolve = await promise;
	return resolve;
}
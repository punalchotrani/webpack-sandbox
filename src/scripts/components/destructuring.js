// Destructuring Assignment

export let [x, y] = ['Foo', 'Bar'];

// Rest pattern [Array]
export let [a, b, c, ...rest] = [100, 200, 300, 400, 500, 600, 700];

// Rest pattern {Objects}
export let { ab, bc, ...restObj } = {
	ab: 'Hello',
	bc: 'World',
	cd: 'Blue',
	df: 'Blah',
	fg: 'Bleh'
};

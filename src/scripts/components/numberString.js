// Numbers & Math Object
const num1 = 100,
  num2 = 200;

let val;

val = num1 + num2;
val = num1 % num2;

// Math object
val = Math.PI;
val = Math.round(8.4);
val = Math.ceil(5.9);
val = Math.floor(1.9);
val = Math.sqrt(89);

val = Math.random(); //This will provide a random number

val = Math.floor(Math.random() * 21); // Whole random number between 0 to 20 with a decimal point.

//console.log(val);


// String and string methods
const firstName = 'Punal';
const lastName = 'Chotrani';
const sentence = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`;

//console.log(sentence);

let fullName = `${firstName} ${lastName}`;

const middleName = 'Suresh';

const newNameFirst = 'Buddy';
const newNameLast = 'Love';

fullName = newNameFirst;
fullName += ' ';
fullName += newNameLast;

val = fullName.indexOf('n');

//get last character
val = fullName.charAt(fullName.length - 1);

val = fullName.slice(1, 10);

val = sentence.replace('.', '').replace('\'s', '').split(' ');

//console.log(val);


// Template literals

const name = 'Punal',
  age = 36,
  job = 'Developer';

let html;

// Without template string
html = '<ul class="center-align"><li>Name: ' + name + '</li><li>Age: ' + age + '</li><li>Job: ' + job + '</li></ul>';

html = `
  <ul class="center-align">
    <li>Name: ${name}</li>
    <li>Age: ${age}</li>
    <li>Job: ${job}</li>
  </ul>`;

let container = document.querySelector('.container');

container.innerHTML += html;
import '../styles/index.scss';
//import { val, numbers } from '../scripts/components/array';

//import val from '../scripts/components/object';

//import val from '../scripts/components/dateTime';

//import val from '../scripts/components/switch';

//import loop from '../scripts/components/loops';

//import createObj from '../scripts/components/loops';
//import { punal, geetu, name1, name2 } from '../scripts/components/oop';

//console.log(numbers);
//console.log(val);
//console.log(this);
//console.log(`${punal.firstName} ${punal.lastName}`);
//console.log(`${geetu.firstName} ${geetu.lastName}`);

//console.log(punal);

//console.log(createObj());
//console.log(loop);

//import { getData } from '../scripts/components/async';

//document.getElementById('button').addEventListener('click', getData);

// import { sayGreeting } from '../scripts/components/async';

//console.log(sayGreeting('Hi there'));

// sayGreeting('Hi There').then(res => console.log(res));

// import { superHeroNames } from '../scripts/components/generators';

// console.log(superHeroNames.next().value);
// console.log(superHeroNames.next().value);
// console.log(superHeroNames.next().value);
// console.log(superHeroNames.next().value);
// console.log(superHeroNames.next().value);
// console.log(superHeroNames.next());

//import { getTheSuperName, getID } from './components/generators';

// console.log(getTheSuperName.next().value.value);
// console.log(getTheSuperName.next().value.value);
// console.log(getTheSuperName.next().value.value);
//console.log(getTheSuperName.next().done);

// console.log(getID.next().value);
// console.log(getID.next().value);
// console.log(getID.next().value);
// console.log(getID.next().value);
// console.log(getID.next().value);
// console.log(getID.next().value);
// console.log(getID.next().value);
// console.log(getID.next().value);

import { getDetails } from './components/generators';
//import { isArray } from 'util';

document.getElementById('next').addEventListener('click', getDetails);

//import { rest, x, y, restObj, ab, bc } from './components/destructuring';

const sum = a => b => (b ? sum(a + b) : a);

const double = a => b => (b ? double(a * b * 2) : a);

//console.log(sum(2)(3)(2)(7)(1)(10)());
//console.log(double(2)(5)());

function likes(names) {
	// TODO
	let newArr, firstTwoItems, outputString;
	if (names.length === 0) {
		return 'no one likes this';
	} else {
		if (names.length <= 3 && names.length > 1) {
			newArr = names.map(name => name);
			newArr.splice(-1, 0, 'and');
			outputString = `${newArr
				.join(', ')
				.replace(', and, ', ' and ')} like this`;
		} else if (names.length >= 4) {
			firstTwoItems = names.map(name => name).slice(0, 2);
			newArr = names.map(name => name);
			outputString = `${firstTwoItems.join(', ')} and ${newArr.length -
				firstTwoItems.length} others like this`;
		} else {
			newArr = names.map(name => name);
			outputString = `${newArr.join()} likes this`;
		}
	}
	return outputString;
}

var favFood = 'pizza';

var foodthought = function() {
	console.log(`${favFood} is my original favourite food`);

	var favFood = 'pasta';

	console.log(`${favFood} is my favourite food`);
};

// foodthought();

function minMax(arr) {
	let emptyArr = [];
	if (Array.isArray(arr)) {
		let sortedArr = arr.sort((a, b) => a - b);
		emptyArr.push(sortedArr[0]);
		emptyArr.push(sortedArr[sortedArr.length - 1]);
	}
	return emptyArr;
}

// minMax([14, 35, 6, 1, 34, 54]);

// function boxes(weights) {
// 	const newArr = [];

// 	if (Array.isArray(weights)) {
// 		let box = 0;
// 		weights.map(weight => {
// 			if (weight + box <= 10) {
// 				//console.log('this is box:', box);
// 				//console.log('this is weight:', weight);
// 				box = box + weight;
// 				console.log('New box weight:', box);
// 			} else {
// 				newArr.push(box);
// 				box = weight;
// 			}
// 		});
// 		//console.log(box);
// 		return box > 0 ? newArr.push(box) : null;
// 	}
// 	//console.log(newArr);
// 	return newArr.length;
// }

//boxes([7, 1, 2, 6, 1, 2, 3, 5, 9, 2, 1, 2, 5]);

//boxes([7, 1, 2, 10, 8, 10, 9]); // 3

// const red = arrData => {
// 	let box = 1;
// 	arrData.reduce((total, amount, index) => {
// 		if (total + amount <= 10) {
// 			return total + amount;
// 		} else {
// 			box++;
// 			return (total = amount);
// 		}
// 	});
// };

// red([7, 1, 2, 6, 1, 2, 3, 5, 9, 2, 1, 2, 5]);

// let tripleAdd = number => number2 => number3 => number + number2 + number3;

//console.log(tripleAdd(10)(10)(10));

// Using spread operator as arguments

const testFunc = (...args) => {
	//console.log(Array.from(arguments));
	//console.log(`1st arg: ${args[0]}`);
	//console.log(`2nd arg: ${args[1]}`);
	//console.log(`3rd arg: ${args[2]}`);
	console.log(args);
};

testFunc('Punal', 'Chotrani', 'Javascript', 'Php');

// Currying

const getName = fName => lName => mName =>
	typeof mName !== 'undefined'
		? `${fName} ${mName} ${lName}`
		: `${fName} ${lName}`;

const myName = getName('Punal')('Chotrani')('Suresh');
console.log(myName);

function yo() {
	let Blah = 'Punal';
	return Blah;
}

let hey = yo();

console.log(hey);
